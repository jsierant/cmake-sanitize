add_library(san INTERFACE)
target_compile_options(
  san INTERFACE
    -fno-omit-frame-pointer
    -O1 -g
)

add_library(asan INTERFACE)
target_link_libraries(asan INTERFACE san)
target_link_options(asan INTERFACE -lasan )
target_compile_options(asan INTERFACE -fsanitize=address)

add_library(ubsan INTERFACE)
target_link_libraries(ubsan INTERFACE san)
target_link_options(ubsan INTERFACE -lubsan)
target_compile_options(ubsan INTERFACE -fsanitize=undefined)
